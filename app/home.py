#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'wangyu'


from bson.objectid import ObjectId
import datetime
from configparser import ConfigParser
from PIL import Image
import io

from .baselib import BaseHandler
from .db import *
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))
from search_server import search

config = ConfigParser()

config.read(os.path.join(os.path.dirname(__file__), 'config.ini'))

LIMIT_PER_PAGE = int(config['Limit']['DEFAULT_LIMIT'])

MONTH = int(config['Time']['MONTH'])


class HomeHandler(BaseHandler):

    @BaseHandler.login
    def get(self):
        skip = self.get_argument('skip', default=0)
        classify = self.get_argument('type', 'latest')
        try:
            skip = int(skip)
            if classify not in ('latest', 'top', 'free', 'monthly', 'advertisement'):
                classify = 'latest'

            if classify == 'latest':
                msg = BaseHandler.currentDB.find({}, sort=[('uptime', -1)], skip=skip).limit(LIMIT_PER_PAGE)
            elif classify == 'top':
                msg = BaseHandler.currentDB.find({}, sort=[('download', -1), ('like', -1)], skip=skip).limit(LIMIT_PER_PAGE)
            elif classify == 'free':
                msg = BaseHandler.currentDB.find({'free': True}, sort=[('uptime', -1)], skip=skip).limit(LIMIT_PER_PAGE)
            elif classify == 'monthly':
                now = datetime.datetime.now()
                start = now - datetime.timedelta(days=30)
                msg = BaseHandler.currentDB.find({'uptime': {'$gt': start, '$lt': now}}, sort=[('download', -1), ('like', -1), ('uptime', -1)], skip=skip).limit(LIMIT_PER_PAGE)
            else:
                msg = advertisement.find({}, sort=[('uptime', -1)], skip=skip).limit(config['Limit']['DEFAULT_LIMIT'])  # TODO render contact and items at here
            msg = self.formatResult(msg)
            pageRange = int(BaseHandler.currentDB.count()/LIMIT_PER_PAGE)
            startPage = int(skip/LIMIT_PER_PAGE)
            if pageRange > startPage+6:
                pageRange = startPage+6
            urls = [{'page': i, 'url': self.context['static_server'] + '/?type=%s' % classify + '&skip=%s' % (i*LIMIT_PER_PAGE)} for i in range(startPage, pageRange)]
            return self.render("home.html", msg=msg, urls=urls, context=self.context['static_server'])
        except Exception as err:
            print(err)
            return self.render("error.html", msg=err)


class SearchHandler(BaseHandler):

    def check_xsrf_cookie(self):
        pass

    # @BaseHandler.login
    def post(self):
        keyword = self.get_argument('keyword', default=None)
        skip = self.get_argument('skip', default=0)
        if not keyword:
            return self.render('notfound.html')

        try:
            es_obj = search.SEARCH()
            es_obj.connect()
            res = es_obj.search_keyword(keyword, skip, LIMIT_PER_PAGE)
            msg = self.formatResult(res['msg'])
            pageRange = int(res['count']/LIMIT_PER_PAGE)
            startPage = int(skip/LIMIT_PER_PAGE)
            if pageRange > startPage+6:
                pageRange = startPage+6
            urls = [{'page': i, 'url': self.context['static_server'] + '/?keyword=%s' % keyword + '&skip=%s' % (i*LIMIT_PER_PAGE)} for i in range(startPage, pageRange)]
            return self.render("home.html", msg=msg, urls=urls, context=self.context['static_server'])
        except Exception as err:
            print(err)
            return self.render("error.html", msg=err, context=self.context['static_server'])


class ImportHandler(BaseHandler):

    def check_xsrf_cookie(self):
        pass

    def post(self):
        appId = self.get_argument('id', default=None)
        if not appId:
            return self.render('notfound.html')

        one = buffers.find_one({'_id': ObjectId(appId)})
        if not one:
            return self.render('notfound.html')
        try:
            if apps.find_one({'appName': one['appName']}):
                return self.render("error.html", msg="Exist", context=self.context['static_server'])
            one['uptime'] = datetime.datetime.now()
            apps.insert(one)
            buffers.remove({'_id': ObjectId(appId)})
            return self.redirect('/')
        except Exception as err:
            print(err)
            return self.render("error.html", msg=err, context=self.context['static_server'])


class DeleteHandler(BaseHandler):

    def check_xsrf_cookie(self):
        pass

    def post(self):
        appId = self.get_argument('id', default=None)
        if not appId:
            return self.render('notfound.html')

        try:
            one = BaseHandler.currentDB.find_one({'_id': ObjectId(appId)})
            if not one:
                return self.render('notfound.html')
            pictureOne = pictures.find_one({'_id': one['pictureId']})
            if pictureOne:
                for x in pictureOne['pictures']:
                    _, value = x.popitem()
                    _, v = value.popitem()
                    picturesFiles.delete(v)
                for i in pictureOne['iconPixes'].values():
                    picturesFiles.delete(i)
                pictures.remove({'_id': one['pictureId']})
            picturesFiles.delete(pictureOne['iconId'])
            files.delete(one['reservesId'])
            BaseHandler.currentDB.remove({'_id': ObjectId(appId)})
            return self.redirect('/')
        except Exception as err:
            print(err)
            return self.render("error.html", msg=err, context=self.context['static_server'])


class UploadPicture(BaseHandler):

    def get(self):
        return self.render("upload_pictures.html", context=self.context['static_server'])

    def check_xsrf_cookie(self):
        pass

    def post(self):
        if self.request.files == {} or 'pictures' not in self.request.files:
            return self.render("error.html", msg='<scrite>alert("select at least one picture")</script>')
        try:
            image_type_list = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/bmp', 'image/x-png']
            files = self.request.files['pictures']
            for image_file in files:
                if image_file['content_type'] not in image_type_list:
                    return self.render("error.html", msg='<script>alert("only support jpg,jpeg,bmp,gif,png Images")</script>')
                if len(image_file['body']) > 10*1024*1024:
                    return self.render("error.html", msg='<script>alert("too big ! contact with developer")</script>')
                picturesFiles.put(image_file['body'])
            return self.redirect('/')
        except Exception as err:
            print(err)
            return self.render("error.html", msg='<script>alert("%s")</script>' % err, context=self.context['static_server'])


class Upload(BaseHandler):

    def get(self):
        return self.render("upload_app.html", context=self.context['static_server'])

    def check_xsrf_cookie(self):
        pass

    def post(self):
        if self.request.files == {} or ('apps' not in self.request.files and 'icon' not in self.request.files and 'pictures' not in self.request.files):
            return self.render("error.html", msg='<script>alert("select at least one apk, one icon and one picture")</script>')

        image_type_list = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/bmp', 'image/x-png']
        imageMap = []
        iconId = ''
        pId = ''
        fId = ''
        iconOringinId = ''
        try:

            pictureF = self.request.files['pictures']
            n = 0

            # starting upload pictures
            for image_file in pictureF:
                if image_file['content_type'] not in image_type_list:
                    return self.render("error.html", msg='<script>alert("only support jpg,jpeg,bmp,gif,png Images")</script>')
                if len(image_file['body']) > 10*1024*1024:
                    return self.render("error.html", msg='<script>alert("too big ! contact with developer")</script>')
                picturePixes = "%sx%s" % Image.open(io.BytesIO(image_file['body'])).size
                pictureId = picturesFiles.put(image_file['body'])
                imageMap.append({'picture%s' % n: {picturePixes: pictureId}})
                n += 1

            # starting upload icon picture
            icons = self.request.files['icon']
            iconpixes = ''

            for icon in icons:
                if icon['content_type'] not in image_type_list:
                    return self.render("error.html", msg='<script>alert("only support jpg,jpeg,bmp,gif,png Images")</script>')
                if len(icon['body']) > 10*1024*1024:
                    return self.render("error.html", msg='<script>alert("too big ! contact with developer")</script>')
                imgHandler = Image.open(io.BytesIO(icon['body']))
                iconpixes = "%sx%s" % imgHandler.size
                rowOutput = io.BytesIO()
                img = imgHandler.resize((512, 512), Image.ANTIALIAS)
                img.save(rowOutput, imgHandler.format)
                iconId = picturesFiles.put(rowOutput.getvalue())
                iconOringinId = picturesFiles.put(icon['body'])

            pId = pictures.insert({"pictures": imageMap, "iconId": iconId, "picturesCount": len(imageMap), "iconPixes": {iconpixes: iconOringinId, '512x512': iconId}})
            # starting upload apk files
            oneFiles = self.request.files['apps']
            for oneFile in oneFiles:
                size = round(len(oneFile['body']) / 1000000.0, 2)
                if size > 20:
                    return self.render("error.html", msg='<script>alert("too big! contact with developer")</script>')
                rtype = oneFile['filename'][-3:]
                noext = oneFile['filename'][:-4]
                fId = files.put(oneFile['body'])
                now = datetime.datetime.now()
                infosrc = {'appName': oneFile['filename'], 'reservesId': fId, 'appSize': str(size)+'MB', 'uptime': now, 'price': 0, 'free': True,
                           'downloads': 0, 'description': '', 'likes': 0, 'appType': rtype, 'developer': '', 'coins': 0,
                           'tag': '', 'title': noext, 'iconId': iconId, 'pictureId': pId, 'views': 0, 'contributor': ''}
                buffers.insert(infosrc)
            return self.redirect('/')
        except Exception as err:
            print(err)
            if fId:
                files.delete(fId)
            if iconId:
                picturesFiles.delete(iconId)
            if imageMap:
                for x in imageMap:
                    _, value = x.popitem()
                    _, v = value.popitem()
                    picturesFiles.delete(v)
            if pId:
                pictures.remove({'_id': pId})
            return self.render("error.html", msg='<script>alert("%s")</script>' % err, context=self.context['static_server'])


class Detail(BaseHandler):

    def get(self):
        try:
            appId = self.get_argument('id', default=None)
            if not appId:
                return self.render('notfound.html')
            exist = BaseHandler.currentDB.find_one({'_id': ObjectId(appId)})
            tempPic = []
            if exist and exist['pictureId']:
                pics = pictures.find_one({'_id': exist['pictureId']})
                if pics:
                    for x in pics['pictures']:
                        _, value = x.popitem()
                        tempPic.append({'url': self.context['static_picture'] + str(value), 'id': str(value)})
                        # _, v = value.popitem()
                        # tempPic.append({'url': self.context['static_picture'] + str(v), 'id': str(v)})
            msg = self.formatDetail(exist)
            if BaseHandler.currentDB == apps:
                online = True
            else:
                online = False
            if msg:
                return self.render("detail.html", msg=msg, pictures=tempPic, online=online, context=self.context['static_server'])
        except Exception as error:
            return self.render("error.html", msg=error, context=self.context['static_server'])


class Property(BaseHandler):

    def get(self):
        appId = self.get_argument('id', default=None)
        if not appId:
            return self.render('notfound.html')
        exist = BaseHandler.currentDB.find_one({'_id': ObjectId(appId)})
        msg = self.formatProperty(exist)
        if msg:
            return self.render("property.html", msg=msg, context=self.context['static_server'])

    def check_xsrf_cookie(self):
        pass

    def post(self):
        appId = self.get_argument('id', default=None)
        appname = self.get_argument('appname', default=None)
        desc = self.get_argument('description', default=None)
        developer = self.get_argument('developer', default=None)
        tag = self.get_argument('tag', default=None)
        title = self.get_argument('title', default=None)
        price = self.get_argument('price', default=None)
        downloads = self.get_argument('downloads', default=None)
        likes = self.get_argument('likes', default=None)

        try:
            if appId:
                appProperty = {}
                if appname:
                    appProperty.update({'appName': appname})
                if desc:
                    appProperty.update({'description': desc})
                if developer:
                    appProperty.update({'developer': developer})
                if tag:
                    appProperty.update({'tag': tag})
                if title:
                    appProperty.update({'title': title})
                if price:
                    appProperty.update({'price': int(price)})
                if downloads:
                    appProperty.update({'downloads': int(downloads)})
                if likes:
                    appProperty.update({'likes': int(likes)})
                if appProperty:
                    BaseHandler.currentDB.update({'_id': ObjectId(appId)}, {'$set': appProperty})
                return self.redirect('/property?id=%s' % appId)
        except Exception as err:
            print(err)
            return self.render("error.html", msg=err, context=self.context['static_server'])


class SwitchDB(BaseHandler):

    def get(self):
        try:
            sdb = self.get_argument('sdb', default=None)
            if sdb == 'buffer':
                BaseHandler.currentDB = buffers
            elif sdb == 'use':
                BaseHandler.currentDB = apps
            else:
                return self.render('notfound.html')
            return self.redirect('/')
        except Exception as err:
            print(err)
            return self.render("error.html", msg=err, context=self.context['static_server'])