#!/usr/bin/python
# coding: utf-8

__author__ = 'wangyu'

import pymongo
import gridfs
import configparser
import os

config = configparser.ConfigParser()

config.read(os.path.join(os.path.dirname(__file__), 'config.ini'))

conn = pymongo.MongoClient(config['Mongo']['HOST'], int(config['Mongo']['PORT']))

decorations = conn[config['Mongo']['DB']]

files = gridfs.GridFS(conn[config['Mongo']['Files']])

picturesFiles = gridfs.GridFS(conn[config['Mongo']['PicturesFiles']])

apps = decorations[config['Mongo']['Apps']]

pictures = decorations[config['Mongo']['Pictures']]

buffers = decorations[config['Mongo']['Buffers']]

advertisement = decorations[config['Mongo']['Advertisement']]
