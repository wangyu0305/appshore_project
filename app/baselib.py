#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'wangyu'

import json
import hashlib
import time
import memcache
import tornado
import tornado.web
import tornado.auth
from tornado import gen
import random
from configparser import ConfigParser

from .db import *

config = ConfigParser()

config.read(os.path.join(os.path.dirname(__file__), 'config.ini'))

STATIC_SERVER = config['Static']['STATIC_SERVER']

FILE_SERVER = config['Static']['FILE_SERVER']

MEM_TIMEOUT = int(config['Time']['WEEK'])

MEMCACHE = memcache.Client([config['Memcache']['MEM_SERVER']], debug=False)


class BaseHandler(tornado.web.RequestHandler):
    currentDB = apps

    def initialize(self):

        self.status = {'id': None, 'login': False}
        self._load_session()
        self.context = {
            'static_server': STATIC_SERVER,
            'static_down': FILE_SERVER+'/download/',
            'static_picture': FILE_SERVER+'/picture/'
        }

    def _load_session(self):
        self.session_id = self.get_secure_cookie('session')
        if self.session_id:
            exist = MEMCACHE.get(self.session_id)
            if exist:
                self.status.update(exist)
            else:
                self.session_id = None

        if not self.session_id:
            self.session_id = self._generate_session_id()
            MEMCACHE.set(self.session_id, self.status, time=MEM_TIMEOUT)
            self.set_secure_cookie('session', self.session_id)

    def _generate_session_id(self):
        sid = ''
        while True:
            randomKey = "wymistery" + str(time.time()) + str(random.random())
            sid = hashlib.md5(randomKey.encode('utf-8')).hexdigest()
            if not MEMCACHE.get(sid):
                break

        return sid

    def write_buffer(self, resp):
        ret = {'resp': resp}
        try:
            _buffer = json.dumps(ret)
        except Exception as what:
            print(what)
            _buffer = {}
        self.set_header('Content-Type', 'application/json')
        return self.write(_buffer)

    @classmethod
    def login(cls, func):
        def wrapper(self, *args, **kwargs):
            if not self.status['login']:
                return self.render('index.html')
            else:
                return func(self, *args, **kwargs)
        return wrapper

    @property
    def db(self):
        return self.application.db

    def formatResult(self, result):
        try:
            smallList = 3
            splitList = lambda longList, n=smallList: [longList[i:i+n] for i in range(0, len(longList), n)]
            alist = [{'appname': res['appName'].strip('.apk').split('.')[-1],
                     'description': res['description'],
                     'id': str(res['_id']),
                     'icon': self.context['static_picture'] + str(res['iconId']),
                     'title': res['title']} for res in result]
            return splitList(alist)
        except Exception as err:
            print(err)
            return [[]]

    def formatProperty(self, result):
        try:
            return {'appname': result['appName'].strip('.apk').split('.')[-1],
                    'description': result['description'],
                    'id': str(result['_id']),
                    'title': result['title'],
                    'price': result['price'],
                    'free': result['free'],
                    'tag': result['tag'],
                    'downloads': result['downloads'],
                    'developer': result['developer'],
                    'likes': result['likes'],
                    'appType': result['appType'],
                    'views': result['views'],
                    'contributor': result['contributor'],
                    'coins': result['coins'],
                    'appSize': result['appSize'],
                    'version': result['version'],
                    'osVersion': result['osVersion']}
        except Exception as err:
            print(err)
            return {}

    def formatDetail(self, result):
        try:
            return {'appname': result['appName'].strip('.apk').split('.')[-1],
                    'description': result['description'],
                    'icon': self.context['static_picture'] + str(result['iconId']),
                    'downloadLink': self.context['static_down'] + str(result['reservesId']),
                    'id': str(result['_id']),
                    'title': result['title'],
                    'price': result['price'],
                    'tag': result['tag'],
                    'downloads': result['downloads'],
                    'developer': result['developer'],
                    'likes': result['likes'],
                    'appType': result['appType'],
                    'views': result['views'],
                    'pictures': result['contributor'],
                    'coins': result['coins'],
                    'version': result['version'],
                    'osVersion': result['osVersion']}
        except Exception as err:
            print(err)
            return {}

    def post(self):
        raise tornado.web.HTTPError(403)

    def get(self):
        raise tornado.web.HTTPError(403)

    def put(self):
        raise tornado.web.HTTPError(403)

    def delete(self):
        raise tornado.web.HTTPError(403)

    def patch(self):
        raise tornado.web.HTTPError(403)


class GoogleOAuth2LoginHandler(tornado.web.RequestHandler,
                               tornado.auth.GoogleOAuth2Mixin):
    @tornado.gen.coroutine
    def get(self):
        if self.get_argument('code', False):
            user = yield self.get_authenticated_user(
                redirect_uri='https://www.decoration.com/auth/google',
                code=self.get_argument('code'))
            # Save the user with e.g. set_secure_cookie
        else:
            yield self.authorize_redirect(
                redirect_uri='https://www.decoration.com/auth/google',
                client_id=self.settings['google_oauth']['key'],
                scope=['profile', 'email'],
                response_type='code',
                extra_params={'approval_prompt': 'auto'})