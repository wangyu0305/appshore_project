__author__ = 'wangyu'

import hashlib
from configparser import ConfigParser
import os
from tornado import gen

from .baselib import BaseHandler, MEMCACHE, MEM_TIMEOUT

config = ConfigParser()

config.read(os.path.join(os.path.dirname(__file__), 'config.ini'))


class Login(BaseHandler):

    def get(self):
        self.render("login.html")

    def check_xsrf_cookie(self):
        pass

    @gen.coroutine
    def post(self):
        username = self.get_argument('username', default=None)
        password = self.get_argument('password', default=None)
        password = hashlib.sha256(password.encode('utf-8')).hexdigest()
        try:
            if username.find('@') > 0:
                verify = 'email'
            else:
                verify = 'username'
            sql = "SELECT (identities) from users where %s='%s' and hash_password='%s';" % (verify, username, password)
            conn = yield self.db.getconn()
            with self.db.manage(conn):
                result = yield conn.execute(sql)
                exist = result.fetchall()
            if exist:
                if exist[0][0] != 'admin':
                    return self.render("create_err.html", msg='you are not one of the member of our admin-console ')
                self.set_login(username)
                return self.redirect('/')
        except Exception as err:
            print(err)

        return self.render('notfound.html')

    def set_login(self, username):
        self.status['login'] = True
        self.status['id'] = hashlib.md5(username.encode('utf-8')).hexdigest()
        MEMCACHE.set(self.session_id, self.status, time=MEM_TIMEOUT)


class Logout(BaseHandler):
    def get(self):
        self.status['login'] = False
        self.status['id'] = ''
        MEMCACHE.set(self.session_id, self.status, time=MEM_TIMEOUT)
        if not self.status['login']:
            return self.render('login.html')


class CreateAccount(BaseHandler):

    def get(self):
        self.render("make_account.html")

    def check_xsrf_cookie(self):
        pass

    @gen.coroutine
    def post(self):
        username = self.get_argument('username', default=None)
        email = self.get_argument('email', default=None)
        password = self.get_argument('password', default=None)
        admin = self.get_argument('admin', default=None)
        password = hashlib.sha256(password.encode('utf-8')).hexdigest()
        try:
            if admin and admin.lower() == config['Admin']['KEY']:
                    if not username.strip():
                        username = email.split('@')[0]
                    sql = "INSERT INTO users (email, username, hash_password, identities, user_id) VALUES ('%s', '%s', '%s', '%s', '%s');" % (email, username, password, 'admin', '123456789')
                    conn = yield self.db.getconn()
                    with self.db.manage(conn):
                        yield conn.execute(sql)
                    return self.redirect('/')
            else:
                msg = 'administrator key error'
            return self.render("create_err.html", msg=msg)
        except Exception as err:
            print(err)
            return self.render("create_err.html", msg=err)
