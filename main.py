#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'wangyu'

import os
import tornado.web
import tornado.httpserver
import tornado.options
import tornado.ioloop
import momoko
from tornado.options import define, options
from configparser import ConfigParser

config = ConfigParser()

config.read('app/config.ini')

from app import home, login


define("port", default=8888, type=int)

options.log_file_prefix = config['Log']['SaveLogFile']


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', home.HomeHandler),
            (r'/import', home.ImportHandler),
            (r'/switch', home.SwitchDB),
            (r'/property', home.Property),
            (r'/detail', home.Detail),
            (r'/search', home.SearchHandler),
            (r'/upload/app', home.Upload),
            (r'/delete', home.DeleteHandler),
            (r'/account', login.CreateAccount),
            (r'/login', login.Login),
            (r'/logout', login.Logout)
        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "template"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            cookie_secret="Thereis@0wayto|find%our#secured(***)!dontdreamit==lol",
            login_url="/login",
            autoescape=None,
            debug=True,
        )

        tornado.web.Application.__init__(self, handlers, **settings)


def main():
    print('... system started ...')
    try:
        tornado.options.parse_command_line()

        ioloop = tornado.ioloop.IOLoop.instance()

        postgre_server = 'dbname=%s user=%s password=%s host=%s port=%s' % (config['Postgresql']['DBNAME'], config['Postgresql']['USER'], config['Postgresql']['PASSWORD'], config['Postgresql']['HOST'], config['Postgresql']['PORT'])

        Application.db = momoko.Pool(dsn=postgre_server, size=1, ioloop=ioloop,)

        # this is a one way to run ioloop in sync
        future = Application.db.connect()
        ioloop.add_future(future, lambda f: ioloop.stop())
        ioloop.start()
        future.result()  # raises exception on connection error

        http_server = tornado.httpserver.HTTPServer(Application(), xheaders=True, ssl_options={
            "certfile": "server.crt",
            "keyfile": "server.key",
        })
        http_server.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
        # tornado.options.parse_command_line()
        #
        # application.listen(options.port)
        # tornado.ioloop.IOLoop.current().start()
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()
